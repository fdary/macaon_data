#! /usr/bin/env bash

function print_usage_and_exit {
  >&2 echo "USAGE : (tsv | txt) expPath [arguments]"
  exit 1
}

MODE=$1
EXPPATH=$2

if [ -z "$MODE" ];
then
  >&2 echo "ERROR : missing argument 1 (mode)"
  print_usage_and_exit
fi

if [ -z "$EXPPATH" ];
then
  >&2 echo "ERROR : missing argument 2 (expPath)"
  print_usage_and_exit
fi

shift
shift

if [ ! -d "$EXPPATH" ]; then
  >&2 echo "ERROR : directory $EXPPATH doesn't exist"
  print_usage_and_exit
fi

TRAIN=$EXPPATH"/data/train.conllu"
TRAINRAW=$EXPPATH"/data/train.txt"
DEV=$EXPPATH"/data/dev.conllu"
DEVRAW=$EXPPATH"/data/dev.txt"
TEST=$EXPPATH"/data/test.conllu"
TESTRAW=$EXPPATH"/data/test.txt"

REF=$TEST
REFRAW=$TESTRAW

if test ! -f $REF;
then
  >&2 echo "ERROR : no ref file found in" $CORPUS
  >&2 echo "$REF"
  print_usage_and_exit
fi
if test ! -f $REFRAW;
then
  >&2 echo "ERROR : no ref file found in" $CORPUS
  >&2 echo "$REFRAW"
  print_usage_and_exit
fi

MCD="ID,FORM,LEMMA,UPOS,XPOS,FEATS,HEAD,DEPREL"
NO=""
for arg in "$@"
do
  if [ "$NO" = "1" ]
  then
    MCD="$arg" 
    NO=""
  fi
  if [ "$arg" = "--mcd" ]
  then
    NO="1"
  fi
done

EVALCONLL="../scripts/conll18_ud_eval.py"
OUTPUT=$EXPPATH"/predicted_eval.tsv"

INPUT="$REF"
INPUTARG="--inputTSV"
if [ "$MODE" = "txt" ]; then
  INPUT="$REFRAW"
  INPUTARG="--inputTXT"
fi

if [ ! -f "$OUTPUT" ]; then
  macaon decode --model $EXPPATH $INPUTARG $INPUT $@ > $OUTPUT || exit 1
fi

$EVALCONLL --mcd $MCD $REF $OUTPUT || exit 1

