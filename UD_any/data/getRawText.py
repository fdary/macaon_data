#! /usr/bin/env python3

import sys
import os
import subprocess

def printUsageAndExit() :
  print("USAGE : %s conll_2_text.pl file1.conllu file2.conllu..."%sys.argv[0], file=sys.stderr)
  exit(1)

if __name__ == "__main__" :
  if len(sys.argv) < 3 :
    printUsageAndExit()

  for pathToFile in sys.argv[2:] :
    splited = os.path.splitext(pathToFile)
    target = splited[0] + ".txt"

    if os.path.isfile(target) :
      continue
    targetFile = open(target, "w")
    command = sys.argv[1] + " " + pathToFile
    p = subprocess.Popen(command, stdout=targetFile, shell=True)
    p.wait()
