#! /usr/bin/env bash

function print_usage_and_exit {
  >&2 echo "USAGE : (tsv | txt) expPath pretrained=col1,col2... [arguments]"
  exit 1
}

MODE=$1
EXPPATH=$2
PRETRAINED=$3

>&2 echo "********************************************************************************"
>&2 echo "Training : "$EXPPATH

if [ -z "$MODE" ];
then
  >&2 echo "ERROR : missing argument 1 (mode)"
  print_usage_and_exit
fi

if [ -z "$EXPPATH" ];
then
  >&2 echo "ERROR : missing argument 2 (expPath)"
  print_usage_and_exit
fi

if [ -z "$PRETRAINED" ];
then
  >&2 echo "ERROR : missing argument 3 (pretrained)"
  print_usage_and_exit
fi

PRETRAINED=$(python3 -c 'import sys; print(" ".join(sys.argv[-1].split("=")[-1].split(",")))' $PRETRAINED)

shift
shift
shift

MCD="ID,FORM,LEMMA,UPOS,XPOS,FEATS,HEAD,DEPREL"
NO=""
for arg in "$@"
do
  if [ "$NO" = "1" ]
  then
    MCD="$arg" 
    NO=""
  fi
  if [ "$arg" = "--mcd" ]
  then
    NO="1"
  fi
done

if [ ! -d "$EXPPATH" ]; then
  >&2 echo "ERROR : directory $EXPPATH doesn't exist"
  print_usage_and_exit
fi

TARGET="all_text"
if [[ "$*" == *--lineByLine* ]]
then
  TARGET="all_lines"
fi

CURDIR=$(pwd)
cd $EXPPATH"/"data && make -s clean && PRETRAINED_COLS=$PRETRAINED MCD=$MCD make $TARGET -s
cd $CURDIR

TRAIN=$EXPPATH"/data/train.conllu"
TRAINRAW=$EXPPATH"/data/train.txt"
DEV=$EXPPATH"/data/dev.conllu"
DEVRAW=$EXPPATH"/data/dev.txt"
TEST=$EXPPATH"/data/test.conllu"
TESTRAW=$EXPPATH"/data/test.txt"

if test ! -f $TRAIN;
then
  pwd
  >&2 echo "ERROR : no train file found in" $EXPPATH
  >&2 echo "$TRAIN"
  print_usage_and_exit
fi

if test ! -f $DEV;
then
	DEV=""
fi

if [ "$MODE" = "txt" ]; then
	if test ! -f $TRAINRAW;
	then
	  >&2 echo "ERROR : no train file found in" $EXPPATH
	  >&2 echo "$TRAINRAW"
	  print_usage_and_exit
	fi
	if test ! -f $DEVRAW;
	then
		DEVRAW=""
	fi
fi

if [ "$MODE" = "tsv" ]; then
macaon train --model $EXPPATH --trainTSV $TRAIN --devTSV $DEV "$@" || exit 1
exit 0
fi

if [ "$MODE" = "txt" ]; then
macaon train --model $EXPPATH --trainTSV $TRAIN --trainTXT $TRAINRAW --devTSV $DEV --devTXT $DEVRAW "$@" || exit 1
exit 0
fi

print_usage_and_exit

