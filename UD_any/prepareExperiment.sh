#! /usr/bin/env bash

source config

function print_usage_and_exit {
  >&2 echo "USAGE : language templateName expName"
  exit 1
}

function has_space {
  [[ "$1" != "${1%[[:space:]]*}" ]] && return 0 || return 1
}

LANG=$1
TEMPLATENAME=$2
EXPNAME=$3

if [ -z "$LANG" ];
then
  >&2 echo "ERROR : missing argument 1 (lang)"
  print_usage_and_exit
fi

if [ -z "$TEMPLATENAME" ];
then
  >&2 echo "ERROR : missing argument 2 (templateName)"
  print_usage_and_exit
fi

if [ -z "$EXPNAME" ];
then
  >&2 echo "ERROR : missing argument 3 (expName)"
  print_usage_and_exit
fi


if [ ! -d "$TEMPLATENAME" ]; then
  >&2 echo "ERROR : directory $TEMPLATENAME doesn't exist"
  print_usage_and_exit
fi

CORPUS=$UD_ROOT"/"$LANG
if [ ! -d "$CORPUS" ]; then
  >&2 echo "ERROR : directory $CORPUS doesn't exist"
  print_usage_and_exit
fi

TRAIN=$(find $CORPUS -name '*train*.conllu')
DEV=$(find $CORPUS -name '*dev*.conllu')
TEST=$(find $CORPUS -name '*test*.conllu')
W2V=$(find $CORPUS -name '*.w2v')

if has_space "$TRAIN" || has_space "$DEV" || has_space "$TEST";
then
  >&2 echo "ERROR : more than 1 match with keyword" $KEYWORD
  >&2 echo "TRAIN : " $TRAIN
  >&2 echo "DEV : " $DEV
  >&2 echo "TEST : " $TEST
  print_usage_and_exit
fi

mkdir -p bin

if [ ! -d "bin/$EXPNAME" ]; then
	cp -r $TEMPLATENAME bin/$EXPNAME
	cp -r "data" bin/$EXPNAME/.
	if [ -f "$TRAIN" ]; then
    ln -s $(readlink -f $TRAIN) bin/$EXPNAME/data/train.conllu
	fi
	if [ -f "$DEV" ]; then
    ln -s $(readlink -f $DEV) bin/$EXPNAME/data/dev.conllu
	fi
	if [ -f "$TEST" ]; then
    ln -s $(readlink -f $TEST) bin/$EXPNAME/data/test.conllu
	fi
	if [ ! -z "$W2V" ]; then
		mkdir -p bin/$EXPNAME/data/W2V/
    ln -s $(readlink -f $W2V) bin/$EXPNAME/data/W2V/
	fi
fi

