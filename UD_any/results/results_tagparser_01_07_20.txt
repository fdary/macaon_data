Corpus             Metric      F1.score        Model                  
--------------------------------------------------------------------------------
UD_Czech-FicTree   LAS         79.91[±0.26]%   tagparser_base         
UD_Czech-FicTree   LAS         80.04[±0.21]%   tagparser_incr         
UD_Czech-FicTree   LAS         81.19[±0.10]%   tagparser_seq          

UD_Czech-FicTree   Lemmas      95.53[±0.02]%   tagparser_incr         
UD_Czech-FicTree   Lemmas      96.05%          tagparser_seq          
UD_Czech-FicTree   Lemmas      96.08[±0.06]%   tagparser_base         

UD_Czech-FicTree   UAS         84.29[±0.34]%   tagparser_base         
UD_Czech-FicTree   UAS         84.44[±0.29]%   tagparser_incr         
UD_Czech-FicTree   UAS         85.69[±0.04]%   tagparser_seq          

UD_Czech-FicTree   UFeats      89.38[±0.07]%   tagparser_base         
UD_Czech-FicTree   UFeats      89.49[±0.05]%   tagparser_incr         
UD_Czech-FicTree   UFeats      89.52[±0.07]%   tagparser_seq          

UD_Czech-FicTree   UPOS        97.10[±0.04]%   tagparser_incr         
UD_Czech-FicTree   UPOS        97.11[±0.01]%   tagparser_seq          
UD_Czech-FicTree   UPOS        97.18[±0.03]%   tagparser_base         
--------------------------------------------------------------------------------
UD_French-GSD      LAS         100.00%         taggerbase             
UD_French-GSD      LAS         100.00%         taglemmaincr           
UD_French-GSD      LAS         84.93[±0.09]%   tagparser_base         
UD_French-GSD      LAS         85.45[±0.36]%   tagparser_incr         
UD_French-GSD      LAS         85.73%          tagparser_seq          
UD_French-GSD      LAS         99.13[±0.07]%   tagparserincrgold      
UD_French-GSD      LAS         99.28[±0.14]%   taggerparserincrgold   

UD_French-GSD      Lemmas      97.06[±0.09]%   tagparserincrgold      
UD_French-GSD      Lemmas      97.45[±0.03]%   tagparser_incr         
UD_French-GSD      Lemmas      97.50[±0.04]%   tagparser_base         
UD_French-GSD      Lemmas      97.59[±0.02]%   tagparser_seq          
UD_French-GSD      Lemmas      97.70[±0.03]%   taglemmaincr           

UD_French-GSD      UAS         87.53[±0.14]%   tagparser_base         
UD_French-GSD      UAS         88.05[±0.36]%   tagparser_incr         
UD_French-GSD      UAS         88.48[±0.06]%   tagparser_seq          

UD_French-GSD      UFeats      92.65[±0.20]%   tagparserincrgold      
UD_French-GSD      UFeats      94.48[±0.09]%   taglemmaincr           
UD_French-GSD      UFeats      95.75[±0.01]%   tagparser_base         
UD_French-GSD      UFeats      95.88[±0.02]%   tagparser_incr         
UD_French-GSD      UFeats      96.05[±0.05]%   tagparser_seq          

UD_French-GSD      UPOS        95.81[±0.13]%   tagparserincrgold      
UD_French-GSD      UPOS        96.40[±0.02]%   taggerparserincrgold   
UD_French-GSD      UPOS        96.50[±0.02]%   taggerbase             
UD_French-GSD      UPOS        96.50[±0.05]%   taglemmaincr           
UD_French-GSD      UPOS        96.93[±0.09]%   tagparser_base         
UD_French-GSD      UPOS        96.95[±0.03]%   tagparser_incr         
UD_French-GSD      UPOS        97.03[±0.07]%   tagparser_seq          
--------------------------------------------------------------------------------
UD_Hebrew-HTB      LAS         69.53[±0.05]%   tagparser_base         
UD_Hebrew-HTB      LAS         69.62[±0.29]%   tagparser_incr         
UD_Hebrew-HTB      LAS         70.54[±0.11]%   tagparser_seq          

UD_Hebrew-HTB      Lemmas      90.89[±0.02]%   tagparser_incr         
UD_Hebrew-HTB      Lemmas      91.72[±0.05]%   tagparser_seq          
UD_Hebrew-HTB      Lemmas      91.82[±0.01]%   tagparser_base         

UD_Hebrew-HTB      UAS         75.35[±0.05]%   tagparser_base         
UD_Hebrew-HTB      UAS         75.52[±0.21]%   tagparser_incr         
UD_Hebrew-HTB      UAS         76.27[±0.03]%   tagparser_seq          

UD_Hebrew-HTB      UFeats      88.25[±0.26]%   tagparser_incr         
UD_Hebrew-HTB      UFeats      89.58[±0.02]%   tagparser_seq          
UD_Hebrew-HTB      UFeats      89.69[±0.17]%   tagparser_base         

UD_Hebrew-HTB      UPOS        90.83[±0.23]%   tagparser_incr         
UD_Hebrew-HTB      UPOS        91.59[±0.00]%   tagparser_seq          
UD_Hebrew-HTB      UPOS        92.06[±0.22]%   tagparser_base         
