Corpus          Metric      F1.score   Model            
--------------------------------------------------------------------------------
UD_French-GSD   LAS         83.51%     tagparser_seq    
UD_French-GSD   LAS         83.77%     tagparser_incr   
UD_French-GSD   LAS         83.81%     tagparser_base   

UD_French-GSD   Sentences   92.11%     tagparser_incr   
UD_French-GSD   Sentences   93.14%     tagparser_seq    
UD_French-GSD   Sentences   93.75%     tagparser_base   

UD_French-GSD   UAS         86.89%     tagparser_seq    
UD_French-GSD   UAS         86.99%     tagparser_incr   
UD_French-GSD   UAS         87.26%     tagparser_base   

UD_French-GSD   UFeats      95.85%     tagparser_base   
UD_French-GSD   UFeats      96.12%     tagparser_incr   
UD_French-GSD   UFeats      96.21%     tagparser_seq    

UD_French-GSD   UPOS        96.74%     tagparser_base   
UD_French-GSD   UPOS        96.78%     tagparser_incr   
UD_French-GSD   UPOS        96.84%     tagparser_seq    
