Corpus            Metric      F1.score   Model                     
--------------------------------------------------------------------------------
UD_Turkish-IMST   LAS         47.33%     dynamictokeparser_incr    
UD_Turkish-IMST   LAS         49.94%     devScoreTokeparser_incr   
UD_Turkish-IMST   LAS         51.26%     tokeparser_incr           
UD_Turkish-IMST   LAS         52.03%     bigtokeparser_incr        
UD_Turkish-IMST   LAS         52.34%     batchtokeparser_incr      

UD_Turkish-IMST   Sentences   96.24%     devScoreTokeparser_incr   
UD_Turkish-IMST   Sentences   96.39%     tokeparser_incr           
UD_Turkish-IMST   Sentences   96.61%     dynamictokeparser_incr    
UD_Turkish-IMST   Sentences   96.76%     batchtokeparser_incr      
UD_Turkish-IMST   Sentences   96.76%     bigtokeparser_incr        

UD_Turkish-IMST   Tokens      99.57%     batchtokeparser_incr      
UD_Turkish-IMST   Tokens      99.58%     devScoreTokeparser_incr   
UD_Turkish-IMST   Tokens      99.59%     dynamictokeparser_incr    
UD_Turkish-IMST   Tokens      99.61%     tokeparser_incr           
UD_Turkish-IMST   Tokens      99.64%     bigtokeparser_incr        

UD_Turkish-IMST   UAS         56.42%     dynamictokeparser_incr    
UD_Turkish-IMST   UAS         58.19%     devScoreTokeparser_incr   
UD_Turkish-IMST   UAS         59.10%     tokeparser_incr           
UD_Turkish-IMST   UAS         59.41%     bigtokeparser_incr        
UD_Turkish-IMST   UAS         60.13%     batchtokeparser_incr      

UD_Turkish-IMST   UFeats      80.90%     dynamictokeparser_incr    
UD_Turkish-IMST   UFeats      83.35%     devScoreTokeparser_incr   
UD_Turkish-IMST   UFeats      85.19%     batchtokeparser_incr      
UD_Turkish-IMST   UFeats      85.62%     tokeparser_incr           
UD_Turkish-IMST   UFeats      85.86%     bigtokeparser_incr        

UD_Turkish-IMST   UPOS        83.56%     dynamictokeparser_incr    
UD_Turkish-IMST   UPOS        86.49%     devScoreTokeparser_incr   
UD_Turkish-IMST   UPOS        88.39%     batchtokeparser_incr      
UD_Turkish-IMST   UPOS        88.59%     tokeparser_incr           
UD_Turkish-IMST   UPOS        89.42%     bigtokeparser_incr        

UD_Turkish-IMST   Words       97.43%     batchtokeparser_incr      
UD_Turkish-IMST   Words       97.46%     devScoreTokeparser_incr   
UD_Turkish-IMST   Words       97.52%     dynamictokeparser_incr    
UD_Turkish-IMST   Words       97.58%     tokeparser_incr           
UD_Turkish-IMST   Words       97.59%     bigtokeparser_incr        
