#! /usr/bin/env python3

import os
import subprocess
import sys
sys.path.insert(1, '../scripts')
from launchSlurmArray import launchSlurmArray

###############################################################################
def printUsageAndExit() :
  print("USAGE : %s (train | eval) (gpu | cpu) nbCPU batchesDescription.py nbHours jobName maxNbSimultaneousJobs"%sys.argv[0], file=sys.stderr)
  exit(1)
###############################################################################

###############################################################################
def prepareExperiment(lang, template, expName) :
  proc = subprocess.Popen("./prepareExperiment.sh %s %s %s"%(lang,template,expName),
      shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  errMess = proc.stderr.read().decode('utf8')
  returnCode = proc.wait()
  if returnCode != 0 : 
    print(errMess, file=sys.stderr)
    exit(1)
###############################################################################

###############################################################################
def addNamesAndCommandsTrain(names, commands, mode, expName, arguments, seed, pretrained) :
  names.append(expName)
  
  commands.append("./train.sh {} bin/{} pretrained={} {} --silent --seed {}".format(mode, expName,pretrained,arguments,seed))
###############################################################################

###############################################################################
def addNamesAndCommandsDecode(names, commands, mode, expName, arguments) :
  names.append(expName)
  
  commands.append("./evaluate.sh {} bin/{} --silent {}".format(mode, expName, arguments))
###############################################################################

###############################################################################
if __name__ == "__main__" :

  if len(sys.argv) != 8 :
    printUsageAndExit()

  mode = sys.argv[1]
  device = sys.argv[2]
  nbCPU = sys.argv[3]
  batchesDescription = sys.argv[4]
  nbHours = sys.argv[5]
  name = sys.argv[6]
  limit = sys.argv[7]

  if mode not in ["train","eval"] or device not in ["cpu","gpu"] :
    printUsageAndExit()

  desc = __import__(os.path.splitext(batchesDescription)[0])

  names = []
  commands = []

  for lang in desc.langs :
    for xp in desc.templatesExperiments :
      pretrained = xp['pretrained'] if "pretrained" in xp else ""
      for i in desc.repRange :
        xp['lang'] = lang
        xp['expName'] = xp['expName'].split('.')[0]+"."+lang+"."+str(i)
        if mode == "train" :
          prepareExperiment(xp['lang'],xp['template'],xp['expName'])
          addNamesAndCommandsTrain(names, commands, xp['mode'],xp['expName'],xp['arguments'],seed=100+i, pretrained=pretrained)
        else :
          addNamesAndCommandsDecode(names, commands, xp['mode'],xp['expName'],xp['evalArguments'])

  launchSlurmArray(names, commands, name, device, nbHours, limit, nbCPU)
###############################################################################

