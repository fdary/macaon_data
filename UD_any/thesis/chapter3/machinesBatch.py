# To untrack the file do : git update-index --skip-worktree batches.py

tagExpPre = [
  {
    'mode'      : 'tsv',
    'expName'   : '%s'%exp,
    'template'  : 'templates/'+exp,
    'arguments' : '-n 20 --trainStrategy 0,ExtractGold,ResetParameters:4,ExtractDynamic,Save --devScore --lockPretrained',
    'pretrained' : '',
    'evalArguments' : ''
  } for (exp) in [("tagparser_incr"), ("tagparser_seq")]
]

tagExpNoPre = [
  {
    'mode'      : 'tsv',
    'expName'   : '%s'%exp,
    'template'  : 'templates/'+exp,
    'arguments' : '-n 20 --trainStrategy 0,ExtractGold,ResetParameters:4,ExtractDynamic,Save --devScore',
    'pretrained' : 'FORM',
    'evalArguments' : ''
  } for (exp) in [("tagparser_incr_nopretrained"), ("tagparser_seq_nopretrained")]
]

templatesExperiments = tagExpPre + tagExpNoPre

langs = [
  "UD_English-EWT",
]

repRange = [0]

