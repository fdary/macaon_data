## Opening useful libraries

library(data.table)
library (dplyr) # Library for ddply
library(lmerTest) # pvalue
library (lme4)
library(sjmisc) #str_contains
library(piecewiseSEM) # R squared
library(ggplot2)

setwd("/home/franck/oculometry/provoNoTok")

################################################################################################
rescaleVariables = function(dataset)
{
	dataset$FREQ = scale(log(dataset$FREQ))
	dataset$WD_LEN = scale(dataset$WD_LEN, center = TRUE, scale = TRUE)
	
	for (colName in colnames(dataset))
	{
	  if (colName != "SENT_ID" & (str_contains(colName, "ENT_") | str_contains(colName, "SUR_")))
	  {
	    dataset[colName] = scale(dataset[colName], center = TRUE, scale = TRUE)
	  }
	}
	
	return(dataset)
}
################################################################################################

################################################################################################
computeCorrelation = function(dataFile)
{
  data = na.omit(read.table(dataFile, header=T, sep="\t", quote="", nrows=-1))
  data = subset(data, FIRST_TIME != 0)
  data = rescaleVariables(data)

  print(paste("CORR ", dataFile, "FREQ"))
  print(cor(data$ENT_CUR_MEAN_TAGGER, data$FREQ), method="pearson")
  print(cor(data$ENT_CUR_MEAN_MORPHO, data$FREQ), method="pearson")
  print(cor(data$ENT_CUR_MEAN_LEMMATIZER_RULES, data$FREQ), method="pearson")
  print(cor(data$ENT_CUR_MEAN_PARSER, data$FREQ), method="pearson")
  print(cor(data$ENT_CUR_MEAN_SEGMENTER, data$FREQ), method="pearson")
  print("")

  print(paste("CORR ", dataFile, "WD_LEN"))
  print(cor(data$ENT_CUR_MEAN_TAGGER, data$WD_LEN), method="pearson")
  print(cor(data$ENT_CUR_MEAN_MORPHO, data$WD_LEN), method="pearson")
  print(cor(data$ENT_CUR_MEAN_LEMMATIZER_RULES, data$WD_LEN), method="pearson")
  print(cor(data$ENT_CUR_MEAN_PARSER, data$WD_LEN), method="pearson")
  print(cor(data$ENT_CUR_MEAN_SEGMENTER, data$WD_LEN), method="pearson")
  print("")
}
################################################################################################


################################################################################################
globalSummary = function(targetCol, dataFile)
{
  data = na.omit(read.table(dataFile, header=T, sep="\t", quote="", nrows=-1))
  data = subset(data, FIRST_TIME != 0)
  data = rescaleVariables(data)

  sink(paste(dataFile, targetCol, "analysis", sep="."), append=T)

  vanillaModel = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + (1 | TEXT_ID/SUBJECT)",sep="")), REML=FALSE, data=data)

  print("#SUMMARY")

  for (ent in c("ENT", "SUR"))
    for (meth in c("CUR"))
      for (mod in c("ADD"))
        for (level in c("TAGGER", "MORPHO", "LEMMATIZER_RULES", "SEGMENTER"))
        {
          measure = paste(ent,meth,mod,level,sep="_")
          formulaStr = paste(targetCol," ~ FREQ + WD_LEN + ",measure," + (1 | TEXT_ID/SUBJECT)",sep="")
          model = lmer(formula(formulaStr), REML=FALSE, data=data)
          print(summary(model))
        }

  for (ent in c("ENT", "SUR"))
    for (meth in c("CUR", "ATT", "TGT"))
      for (mod in c("ADD", "MEAN", "MAX"))
        for (level in c("PARSER"))
        {
          measure = paste(ent,meth,mod,level,sep="_")
          formulaStr = paste(targetCol," ~ FREQ + WD_LEN + ",measure," + (1 | TEXT_ID/SUBJECT)",sep="")
          model = lmer(formula(formulaStr), REML=FALSE, data=data)
          print(summary(model))
        }

  sink()
  print(paste("SUMMARY", targetCol, dataFile))
}
################################################################################################

################################################################################################
globalAnova = function(targetCol, dataFile)
{
  data = na.omit(read.table(dataFile, header=T, sep="\t", quote="", nrows=-1))
  data = subset(data, FIRST_TIME != 0)
  data = rescaleVariables(data)

  sink(paste(dataFile, targetCol, "analysis", sep="."))

  vanillaModel = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + (1 | TEXT_ID/SUBJECT)",sep="")), REML=FALSE, data=data)

  EntCurAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_CUR_ADD_TAGGER + ENT_CUR_ADD_MORPHO + ENT_CUR_ADD_LEMMATIZER_RULES + ENT_CUR_ADD_PARSER + ENT_CUR_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntCurMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_CUR_MEAN_TAGGER + ENT_CUR_MEAN_MORPHO + ENT_CUR_MEAN_LEMMATIZER_RULES + ENT_CUR_MEAN_PARSER + ENT_CUR_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntCurMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_CUR_MAX_TAGGER + ENT_CUR_MAX_MORPHO + ENT_CUR_MAX_LEMMATIZER_RULES + ENT_CUR_MAX_PARSER + ENT_CUR_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)

  EntAttAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_ATT_ADD_TAGGER + ENT_ATT_ADD_MORPHO + ENT_ATT_ADD_LEMMATIZER_RULES + ENT_ATT_ADD_PARSER + ENT_ATT_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntAttMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_ATT_MEAN_TAGGER + ENT_ATT_MEAN_MORPHO + ENT_ATT_MEAN_LEMMATIZER_RULES + ENT_ATT_MEAN_PARSER + ENT_ATT_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntAttMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_ATT_MAX_TAGGER + ENT_ATT_MAX_MORPHO + ENT_ATT_MAX_LEMMATIZER_RULES + ENT_ATT_MAX_PARSER + ENT_ATT_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)

  EntTgtAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_TGT_ADD_TAGGER + ENT_TGT_ADD_MORPHO + ENT_TGT_ADD_LEMMATIZER_RULES + ENT_TGT_ADD_PARSER + ENT_TGT_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntTgtMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_TGT_MEAN_TAGGER + ENT_TGT_MEAN_MORPHO + ENT_TGT_MEAN_LEMMATIZER_RULES + ENT_TGT_MEAN_PARSER + ENT_TGT_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  EntTgtMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + ENT_TGT_MAX_TAGGER + ENT_TGT_MAX_MORPHO + ENT_TGT_MAX_LEMMATIZER_RULES + ENT_TGT_MAX_PARSER + ENT_TGT_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)

  SurCurAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_CUR_ADD_TAGGER + SUR_CUR_ADD_MORPHO + SUR_CUR_ADD_LEMMATIZER_RULES + SUR_CUR_ADD_PARSER + SUR_CUR_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurCurMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_CUR_MEAN_TAGGER + SUR_CUR_MEAN_MORPHO + SUR_CUR_MEAN_LEMMATIZER_RULES + SUR_CUR_MEAN_PARSER + SUR_CUR_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurCurMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_CUR_MAX_TAGGER + SUR_CUR_MAX_MORPHO + SUR_CUR_MAX_LEMMATIZER_RULES + SUR_CUR_MAX_PARSER + SUR_CUR_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)

  SurAttAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_ATT_ADD_TAGGER + SUR_ATT_ADD_MORPHO + SUR_ATT_ADD_LEMMATIZER_RULES + SUR_ATT_ADD_PARSER + SUR_ATT_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurAttMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_ATT_MEAN_TAGGER + SUR_ATT_MEAN_MORPHO + SUR_ATT_MEAN_LEMMATIZER_RULES + SUR_ATT_MEAN_PARSER + SUR_ATT_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurAttMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_ATT_MAX_TAGGER + SUR_ATT_MAX_MORPHO + SUR_ATT_MAX_LEMMATIZER_RULES + SUR_ATT_MAX_PARSER + SUR_ATT_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)

  SurTgtAdd = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_TGT_ADD_TAGGER + SUR_TGT_ADD_MORPHO + SUR_TGT_ADD_LEMMATIZER_RULES + SUR_TGT_ADD_PARSER + SUR_TGT_ADD_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurTgtMean = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_TGT_MEAN_TAGGER + SUR_TGT_MEAN_MORPHO + SUR_TGT_MEAN_LEMMATIZER_RULES + SUR_TGT_MEAN_PARSER + SUR_TGT_MEAN_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  SurTgtMax = lmer(formula(paste(targetCol," ~ FREQ + WD_LEN + SUR_TGT_MAX_TAGGER + SUR_TGT_MAX_MORPHO + SUR_TGT_MAX_LEMMATIZER_RULES + SUR_TGT_MAX_PARSER + SUR_TGT_MAX_SEGMENTER + (1 | TEXT_ID/SUBJECT)",sep="")) , REML=FALSE, data=data)
  
  print("#ANOVA")
  print(anova(vanillaModel, EntCurAdd, EntCurMean, EntCurMax, EntAttAdd, EntAttMean, EntAttMax, EntTgtAdd, EntTgtMean, EntTgtMax, SurCurAdd, SurCurMean, SurCurMax, SurAttAdd, SurAttMean, SurAttMax, SurTgtAdd, SurTgtMean, SurTgtMax))

  sink()
  print(paste("ANOVA", targetCol, dataFile))
}
################################################################################################

################################################################################################
computeCorrelation("outputs/UD_English-EWT/incr_pretrained_times.tsv")
computeCorrelation("outputs/UD_English-EWT/incr_nopretrained_times.tsv")


globalAnova("FIRST_TIME","outputs/UD_English-EWT/incr_nopretrained_times.tsv")
globalAnova("FIRST_TIME","outputs/UD_English-EWT/incr_pretrained_times.tsv")
globalAnova("FIRST_TIME","outputs/UD_English-EWT/seq_nopretrained_times.tsv")
globalAnova("FIRST_TIME","outputs/UD_English-EWT/seq_pretrained_times.tsv")


globalAnova("TOTAL_TIME","outputs/UD_English-EWT/incr_nopretrained_times.tsv")
globalAnova("TOTAL_TIME","outputs/UD_English-EWT/incr_pretrained_times.tsv")
globalAnova("TOTAL_TIME","outputs/UD_English-EWT/seq_nopretrained_times.tsv")
globalAnova("TOTAL_TIME","outputs/UD_English-EWT/seq_pretrained_times.tsv")


globalSummary("FIRST_TIME","outputs/UD_English-EWT/incr_nopretrained_times.tsv")
globalSummary("FIRST_TIME","outputs/UD_English-EWT/incr_pretrained_times.tsv")
globalSummary("FIRST_TIME","outputs/UD_English-EWT/seq_nopretrained_times.tsv")
globalSummary("FIRST_TIME","outputs/UD_English-EWT/seq_pretrained_times.tsv")


globalSummary("TOTAL_TIME","outputs/UD_English-EWT/incr_nopretrained_times.tsv")
globalSummary("TOTAL_TIME","outputs/UD_English-EWT/incr_pretrained_times.tsv")
globalSummary("TOTAL_TIME","outputs/UD_English-EWT/seq_nopretrained_times.tsv")
globalSummary("TOTAL_TIME","outputs/UD_English-EWT/seq_pretrained_times.tsv")

