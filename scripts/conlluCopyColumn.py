#! /usr/bin/env python3

import sys
from readMCD import readMCD

if len(sys.argv) < 4 :
  print("USAGE : %s fromColumn toColumn file1.conllu file2.conllu..."%sys.argv[0], file=sys.stderr)
  exit(1)

fromCol = sys.argv[1]
toCol = sys.argv[2]

for filename in sys.argv[3:] :
  lines = []
  for line in open(filename, "r") :
    line = line.strip()
    if "# global.columns =" in line :
      line = line + " " + toCol
      conllMCD, conllMCDr = readMCD(line.split('=')[-1].strip())
    if len(line) == 0 or line[0] == '#' :
      lines.append(line)
      continue
    splited = line.split('\t')
    fromValue = splited[conllMCD[fromCol]]
    splited.append(fromValue)
    lines.append("\t".join(splited))
  with open(filename, "w") as out :
    print("\n".join(lines), file=out)

