#! /usr/bin/env python3

import sys
from readMCD import readMCD

################################################################################
def printUsageAndExit() :
  print("USAGE : %s embeddings.w2v vocabFile1.conllu vocabFile2.conllu..."
    %sys.argv[0], file=sys.stderr)
  exit(1)
################################################################################

################################################################################
if __name__ == "__main__" :
  if len(sys.argv) < 3 :
    printUsageAndExit()

  baseMCD = "ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC"

  vocab = {}

  for filename in sys.argv[2:] :
    col2index, index2col = readMCD(baseMCD)
    for line in open(filename, "r") :
      line = line.strip()
      if len(line) == 0 :
        continue
      if "# global.columns =" in line :
        col2index, index2col = readMCD(line.split('=')[-1].strip())
        continue
      if line[0] == '#' :
        continue
      word = line.split('\t')[col2index["FORM"]]
      vocab[word] = True
    
  print("Vocabulary size = %d words"%len(vocab), file=sys.stderr)
  
  for line in open(sys.argv[1]) :
    line = line.strip()
    splited = line.split(' ')
    # Ignore optional w2v header
    if len(splited) == 2 :
      continue
    word = splited[0]
    if word in vocab :
      print(line)
################################################################################

