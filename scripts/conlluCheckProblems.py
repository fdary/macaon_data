#! /usr/bin/env python3

import sys
from readMCD import readMCD

headColName = "HEAD"
deprelColName = "DEPREL"
idColName = "ID"

################################################################################
def printUsageAndExit() :
  print("USAGE : %s file.conllu"%sys.argv[0], file=sys.stderr)
  exit(1)
################################################################################

################################################################################
def checkMCD(mcd) :
  for col in [headColName, deprelColName, idColName] :
    if col not in mcd :
      print("ERROR : column '{}' missing from mcd '{}'"
        .format(col, " ".join(mcd.keys())), file=sys.stderr)
      exit(1)
################################################################################

################################################################################
def logError(message, sentence) :
  print(message, file=sys.stderr)
  for line in sentence :
    for col in line :
      print(col,end="\t", file=sys.stderr)
    print("", file=sys.stderr)
################################################################################

################################################################################
def checkSentence(fileLineIndex, sentence, conllMCD, conllMCDr) :
  idIndex = int(conllMCDr[idColName])
  govIndex = int(conllMCDr[headColName])
  labelIndex = int(conllMCDr[deprelColName])

  curId = 0
  curDigit = 1
  maxId = 0
  multiWordEmptyNodes = set()
  id2index = {}

  hadErr = False
# Verifying IDS
  for i in range(len(sentence)) :

    for col in sentence[i] :
      if len(col) == 0 :
        logError("Empty column on line %s"%(fileLineIndex+i), sentence)
        return False

    idStr = sentence[i][idIndex]
    if idStr in id2index :
      logError("ERROR in IDs : line %s '%s' already seen"%(fileLineIndex+i,idStr), sentence)
      hadErr = True

    id2index[idStr] = i

    if len(idStr.split('-')) == 2 :
      curDigit = 1
      splited = idStr.split('-')
      multiWordEmptyNodes.add(i)
      if int(splited[0]) != curId+1 or int(splited[0]) >= int(splited[1]) :
        logError("ERROR in line %s, IDs : %s"%(fileLineIndex+i,idStr), sentence)
        hadErr = True
    elif len(idStr.split('.')) == 2 :
      multiWordEmptyNodes.add(i)
      splited = idStr.split('.')
      if int(splited[0]) != curId or int(splited[1]) != curDigit :
        logError("ERROR in line %s, IDs : %s"%(fileLineIndex+i,idStr), sentence)
        hadErr = True
      curDigit += 1
    elif idStr.isdigit() :
      curId += 1
      curDigit = 1
      maxId = max(maxId, int(idStr))
      if int(idStr) != curId :
        logError("ERROR in line %s, IDs : %s"%(fileLineIndex+i,idStr), sentence)
        hadErr = True
    else :
      logError("ERROR in line %s, IDs : %s"%(fileLineIndex+i,idStr), sentence)
      hadErr = True

  nbRoot = 0
# Verifying root
  for i in range(len(sentence)) :
    labelStr = sentence[i][labelIndex]
    if labelStr == "root" :
      nbRoot += 1

  if nbRoot != 1 :
    logError("ERROR %d root in sentence on line %s:"%(nbRoot,fileLineIndex), sentence)
    hadErr = True

# Verifying govs
  for i in range(len(sentence)) :
    if i in multiWordEmptyNodes :
      continue
    govStr = sentence[i][govIndex]
    if not govStr.isdigit() :
      logError("ERROR line %d gov \'%s\' is not positive integer :"%(fileLineIndex+i,govStr), sentence)
      hadErr = True

    if int(govStr) > maxId :
      logError("ERROR line %d gov \'%s\' is out of sentence :"%(fileLineIndex+i,govStr), sentence)
      hadErr = True

# Verifying cycles
  alreadyReported = {}
  for i in range(len(sentence)) :
    if i in multiWordEmptyNodes :
      continue
    alreadySeen = {}
    currentNode = i
    while True :
      alreadySeen[currentNode] = True
      govStr = sentence[currentNode][govIndex]
      if govStr == "0" :
        break
      currentNode = id2index[govStr]
      if currentNode in alreadySeen :
        if currentNode not in alreadyReported :
          logError("ERROR line %d (id=%s) loop in governors :"%(fileLineIndex+currentNode, sentence[currentNode][idIndex]), sentence)
          hadErr = True
          alreadyReported[currentNode] = True
        break

  return not hadErr
################################################################################


################################################################################
if __name__ == "__main__" :
  if len(sys.argv) != 2 :
    printUsageAndExit()

  baseMCD = "ID FORM LEMMA POS XPOS FEATS HEAD DEPREL"
  conllMCD, conllMCDr = readMCD(baseMCD)
  checkMCD(conllMCD)

  sentence = []
  fileLineIndex = 0
  sentFirstLine = -1

  for line in open(sys.argv[1], "r", encoding="utf8") :
    fileLineIndex += 1
    clean = line.strip()

    if len(clean) < 3 :
      if sentFirstLine == -1 :
        exit(1)
      if checkSentence(sentFirstLine, sentence, conllMCDr, conllMCD) :
        for line in sentence :
          print("\t".join(line))
        print("")
      sentence = []
      sentFirstLine = -1
      continue
    if clean[0] == '#' :
      splited = line.split("global.columns =")
      if len(splited) > 1 :
        print(line.strip())
        conllMCD, conllMCDr = readMCD(splited[-1].strip())
        checkMCD(conllMCD)
      continue
    if sentFirstLine == -1 :
      sentFirstLine = fileLineIndex
    sentence.append(clean.split('\t'))
################################################################################

