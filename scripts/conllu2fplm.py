#! /usr/bin/env python3

import sys
from readMCD import readMCD

def printUsageAndExit() :
  print("USAGE : %s file.conllu"%sys.argv[0], file=sys.stderr)
  exit(1)

def sameLineWithoutLemma(l1, l2) :
  l1s = l1.split('\t')
  l2s = l2.split('\t')
  return (l1s[:-3],l1s[-1]) == (l2s[:-3],l2s[-1])

if __name__ == "__main__" :

  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) != 2 :
    printUsageAndExit()

  col2index, index2col = readMCD("ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC")

  entriesCount = {}
  entriesList = []

  for line in open(sys.argv[1], "r", encoding="utf8") :
    if len(line.strip()) < 3 :
      continue
    if line.strip()[0] == '#' :
      splited = line.split("global.columns =")
      if len(splited) > 1 :
        col2index, index2col = readMCD(splited[-1].strip())
      continue

    columns = line.strip().split('\t')
    if len(columns[col2index["ID"]].split('-')) > 1 :
      continue

    entry = ""
    for col in ["FORM", "UPOS", "LEMMA", "FEATS"] :
      entry = entry + (columns[col2index[col]] if col in col2index else "_") + '\t'
    entry = entry[:-1]

    if entry not in entriesCount :
      entriesCount[entry] = 1
    else :
      entriesCount[entry] = 1+entriesCount[entry]

  for entry in entriesCount :
    entriesList.append(entry)

  entriesList.sort()
  i = 0
  while i < len(entriesList) :
    maxCount = 0
    maxIndex = 0
    j = i
    while j < len(entriesList) and sameLineWithoutLemma(entriesList[i], entriesList[j]) :
      if entriesCount[entriesList[j]] > maxCount :
        maxCount = entriesCount[entriesList[j]]
        maxIndex = j
      j = j+1
    print("%s"%(entriesList[maxIndex]))
    i = j

