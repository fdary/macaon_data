#! /usr/bin/env python3

import sys
from readMCD import readMCD

rules = {}
prefix = "SPLITWORD "

def printUsageAndExit() :
  print("USAGE : %s file.conllu [mcd]"%sys.argv[0], file=sys.stderr)
  exit(1)

def computeRules(sentence) :
  wordById = {}
  for word in sentence :
    splited = word[0].split("-")
    if len(splited) > 1 :
      continue
    wordById[word[0]] = word[1]

  for word in sentence :
    splited = word[0].split("-")
    if len(splited) > 1 :
      rule = ""
      for id in range(int(splited[0]),int(splited[-1])+1) :
        rule += "@" + wordById[str(id)]
      if word[1] in rules :
        if rule in rules[word[1]] :
          rules[word[1]][rule] += 1
        else :
          rules[word[1]][rule] = 1
      else :
        rules[word[1]] = {}
        rules[word[1]][rule] = 1

def main() :
  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) < 2 :
    printUsageAndExit()
  if len(sys.argv) == 3 :
    mcd = sys.argv[2].replace(",", " ")
  else :
    mcd = "ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC"

  col2index, index2col = readMCD(mcd)

  sentence = []

  for line in open(sys.argv[1], "r", encoding="utf8") :
    if len(line.strip()) < 2 or line[0] == '#' :
      splited = line.split("global.columns =")
      if len(splited) > 1 :
        col2index, index2col = readMCD(splited[-1].strip())
      if len(sentence) > 0 :
        computeRules(sentence)
      sentence = []
      continue

    if "ID" not in col2index or "FORM" not in col2index :
      break

    idId = int(col2index["ID"])
    idForm = int(col2index["FORM"])

    splited = line.strip().split('\t')
    sentence += [[splited[idId], splited[idForm]]]

  for word in rules :
    if len(rules[word]) > 1 :
      print("WARNING : Abiguity detected in \'%s\'"%(word+" "+str(rules[word])), file=sys.stderr)
    toPrint = []
    for rule in rules[word] :
      toPrint.append([len(rule.split('@')), prefix+word+rule])
    toPrint.sort(reverse=True)
    for rule in toPrint :
      print(rule[1])

if __name__ == "__main__" :
  main()

