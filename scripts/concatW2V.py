#! /usr/bin/env python3

import sys

nbLines = 0
embSize = None
for filename in sys.argv[1:] :
  for line in open(filename, "r") :
    line = line.strip()
    splited = line.split()
    if len(splited) == 2 :
      if embSize is None :
        embSize = int(splited[1])
      elif embSize != int(splited[1]) :
        print("ERROR : incompatibles embedings sizes %d and %d"%(embSize, int(splited[1])), file=sys.stderr)
        exit(1)
    else :
      nbLines += 1

print(nbLines, embSize)
for filename in sys.argv[1:] :
  prefix = filename.split('/')[-1].split('.')[0]
  for line in open(filename, "r") :
    line = line.strip()
    splited = line.split()
    if len(splited) > 2 :
      print(prefix+"_"+line)

