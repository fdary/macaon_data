#! /usr/bin/env python3

import sys

################################################################################
def printUsageAndExit() :
  print("USAGE : %s infos.tsv baseFile.tsv keyColumnIndex defaultSymbol"%sys.argv[0], file=sys.stderr)
  exit(1)
################################################################################

################################################################################
def readInfos(filename) :
  infos = {}
  usualColNumber = -1
  for line in open(filename, 'r') :
    if len(line) == 0 or '\t' not in line or line[0] == '#' :
      continue
    splited = line.strip().split('\t')
    if usualColNumber == -1 :
      usualColNumber = len(splited)
    if len(splited) != usualColNumber :
      print("WARNING : expected %d columns for line '%s'"%(usualColNumber, line.strip()), file=sys.stderr)
      continue
    if splited[0] in infos :
      print("ERROR : redefinition of line '%s'"%line.strip(), file=sys.stderr)
      exit(1)
    infos[splited[0]] = splited[1:]

  return infos
################################################################################

################################################################################
def pasteInfosToFile(filename, infos, colIndex, defaultSymbol) :
  nbColsToAdd = 0
  for key in infos :
    nbColsToAdd = len(infos[key])
    break

  for line in open(filename, 'r') :
    if len(line) == 0 or '\t' not in line or line[0] == '#' :
      print(line.strip())
      continue
    splited = line.strip().split('\t')
    if colIndex not in range(0,len(splited)) :
      print("ERROR : column index '%d' not found in line '%s'"%(colIndex, line.strip()), file=sys.stderr)
      exit(1)
    if splited[colIndex] in infos :
      for col in infos[splited[colIndex]] :
        splited.append(col)
    else :
      for i in range(nbColsToAdd) :
        splited.append(defaultSymbol)
    print("\t".join(splited))
################################################################################

################################################################################
if __name__ == "__main__" :
  if len(sys.argv) != 5 :
    printUsageAndExit()

  infoFile = sys.argv[1]
  baseFile = sys.argv[2]
  colIndex = int(sys.argv[3])
  defaultSymbol = sys.argv[4]

  infos = readInfos(infoFile)

  pasteInfosToFile(baseFile, infos, colIndex, defaultSymbol)
################################################################################

